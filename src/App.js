import React, { useState } from 'react';
import './App.css';
import { VscArrowLeft } from "react-icons/vsc";
import { useAlert } from 'react-alert'

import BlackVersion from './assets/images/ath-msr7-black.jpg';
import BrownVersion from './assets/images/ath-msr7-brown.jpg';

const descriptionbody=`Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                      when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                      It has survived not only five centuries, but also the leap into electronic typesetting, 
                      remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                      sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                      Aldus PageMaker including versions of Lorem Ipsum`
                      
const detailsbody=`There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration 
                    in some form, by injected humour, or randomised words which don't look even slightly believable. 
                    If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. 
                    All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, 
                    making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, 
                    combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.
                    The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.`
const variants=[
  {
    key:'1',
    value:'Black',
    image: BlackVersion
  },
  {
    key:'2',
    value:'Brown',
    image: BrownVersion
  }
];

function App() {
  const alertMessage=useAlert();
  const [variant, setVariant] = useState(BlackVersion);
  const [tab, setActiveTab] = useState('description');
  const [cartButtonLabel, setCartButtonLabel] = useState('ADD TO CART');

  const handleSetVariant = (e) => setVariant(variants.find(x => x.key === e.target.value).image);
  const handleAddToCart = () => {
    if(cartButtonLabel==='ADD TO CART'){
      setCartButtonLabel('LOADING'); 
      setTimeout(() => {
        alertMessage.success('Item added to cart!',{ timeout: 1500})
        setCartButtonLabel('VIEW CART');
      }, 2 * 1000); 
    }
  }

  return (
    <div className="App">
      <div className="left-container">
        <div className="nav-header">
          <div className="icon">
            <VscArrowLeft />
          </div>
          <label>
            All products
          </label>
        </div>
        <div className="product-title-container">
          <div className="product-title">Audio-Technica ATH-MSR7</div>
          <div className="product-sub-title">2017 Best HeadPhones of the Year Award Winner</div>
        </div>
        <div className="tabs">
          <span className={`links ${tab==='description' && 'active'}`} onClick={()=>setActiveTab('description')}>
            DESCRIPTION
          </span>
          <span className={`links ${tab==='details' && 'active'}`} onClick={()=>setActiveTab('details')}>
            DETAILS
          </span>
        </div>
        <div className="tab-values">
          <div className="value-container">
            <div className="value-text">
              {tab==='description'?descriptionbody:detailsbody}
            </div>
            <div className="price-container">
              <div  className="discounted-price">
                <span>
                  $59.99
                </span>
              </div>
              <div className="original-price">
                <span>
                  $89.99
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="variant-selector-container">
          <label>COLORS</label>
          <div className="custom-select">
            <select
              onChange={e => handleSetVariant(e)}>
              {
                variants.map(({key,value})=> <option key={key} value={key}>{value}</option>)
              }
            </select >
          </div>
          </div>
        <div className="button-container">
          <div className="button" onClick={()=>handleAddToCart()}>{cartButtonLabel}</div>
        </div>
      </div>
      <div className="right-container">
        <div className="image-container">
          <img alt="black" src={variant}/>
        </div>
      </div>
    </div>
  );
}

export default App;
